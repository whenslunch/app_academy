require 'singleton'
require 'sqlite3'


class QuestionsDatabase < SQLite3::Database
  include Singleton

  def initialize
    super('questions.db')
    self.results_as_hash = true
    self.type_translation = true
  end
end

class User
  attr_reader :fname, :lname

  def initialize(options = {})
    @id = options["id"]
    @fname = options["fname"]
    @lname = options["lname"]
    create
  end

  def fname=(new_fname)
    return if self.id.nil?
    @fname = new_fname
    QuestionsDatabase.instance.execute(<<-SQL, @fname, @id)
      UPDATE users
      SET fname = ?
      WHERE id = ?
    SQL
  end

  def lname=(new_lname)
    return if self.id.nil?
    @lname = new_lname
    QuestionsDatabase.instance.execute(<<-SQL, @lname, @id)
      UPDATE users
      SET lname = ?
      WHERE id = ?
    SQL
  end

  def create
    return unless self.id.nil?
    QuestionsDatabase.instance.execute(<<-SQL, @fname, @lname)
      INSERT INTO users (fname,lname)
      VALUES (?, ?)
    SQL

    @id = QuestionsDatabase.instance.last_insert_row_id
  end

  def followed_questions
    QuestionFollower.followed_questions_for_user_id(@id)
  end

  def self.find_by_id(id)
    QuestionsDatabase.instance.execute(<<-SQL, id)
    SELECT *
    FROM users
    WHERE users.id = ?
    SQL
  end

  def authored_questions
    Question.find_by_author_id(@id)
  end

  def self.find_by_name(fname, lname)
    QuestionsDatabase.instance.execute(<<-SQL, fname, lname)
    SELECT *
    FROM users
    WHERE users.fname = ? && user.lname = ?
    SQL
  end

  def authored_replies
    Reply.find_by_author_id(@id)
  end

  def liked_questions
    QuestionLikes.liked_questions_for_user_id(@id)
  end

  def average_karma
    QuestionsDatabase.instance.execute(<<-SQL, @id)
    SELECT AVG(likes)
    FROM
    (
      SELECT COUNT(question_likes.user_id) AS likes
      FROM questions
      LEFT JOIN question_likes ON questions.question_id = question_likes.question_id
      WHERE questions.user_id = 2
      GROUP BY questions.question_id
    )
    SQL
  end

end

class Question
  attr_reader :title, :body, :user

  def initialize(options = {})
    @question_id = options["question_id"]
    @title = options["title"]
    @body = options["body"]
    @user = options["user_id"]
    create
  end

  def title=(new_title)
    return if self.question_id.nil?
    @title = new_title
    QuestionsDatabase.instance.execute(<<-SQL, @title, @question_id)
      UPDATE questions
      SET title = ?
      WHERE question_id = ?
    SQL
  end

  def body=(new_body)
    return if self.question_id.nil?
    @body = new_body
    QuestionsDatabase.instance.execute(<<-SQL, @body, @question_id)
      UPDATE questions
      SET body = ?
      WHERE question_id = ?
    SQL
  end

  def create
    return unless self.id.nil?
    QuestionsDatabase.instance.execute(<<-SQL, @title, @body, @user)
      INSERT INTO questions (title, body, user_id)
      VALUES (?, ?, ?)
    SQL

    @id = QuestionsDatabase.instance.last_insert_row_id
  end

  def self.find_by_id(question_id)
    QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT *
    FROM questions
    WHERE questions.question_id = ?
    SQL
  end

  def self.find_by_author_id(user_id)
    QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT *
    FROM questions
    WHERE questions.user_id = ?
    SQL
  end

  def author
    User.find_by_id(@user)
  end

  def replies
    Reply.find_by_question_id(@question_id)
  end

  def followers
    QuestionFollower.followers_for_question_id(@question_id)
  end

  def likers
    QuestionLikes.likers_for_question_id(@question_id)
  end

  def num_likes
    QuestionLikes.num_likes_for_question(@question_id)
  end

  def self.most_liked(n)
    QuestionLikes.most_liked_questions(n)
  end

end

class Reply
  attr_reader :user, :question, :root_reply, :body

  def initialize(options = {})
    @reply_id = options["reply_id"]
    @user = options["user_id"]
    @question = options["question_id"]
    @root_reply = options["root_reply"]
    @body = options["body"]
    create
  end

  def body=(new_body)
    return if self.reply_id.nil?
    @body = new_body
    QuestionsDatabase.instance.execute(<<-SQL, @body, @reply_id)
      UPDATE replies
      SET body = ?
      WHERE reply_id = ?
    SQL
  end

  def create
    return unless self.reply_id.nil?
    QuestionsDatabase.instance.execute(<<-SQL, @user, @question, @root_reply, @body)
      INSERT INTO replies (user_id, question_id, root_reply, body)
      VALUES (?, ?, ?, ?)
    SQL

    @id = QuestionsDatabase.instance.last_insert_row_id
  end

  def self.find_by_question_id(question_id)
    QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT *
    FROM replies
    WHERE replies.question_id = ?
    SQL
  end

  def self.find_by_id(reply_id)
    QuestionsDatabase.instance.execute(<<-SQL, reply_id)
    SELECT *
    FROM replies
    WHERE replies.reply_id = ?
    SQL
  end

  def self.find_by_user_id(user_id)
    QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT *
    FROM replies
    WHERE replies.user_id = ?
    SQL
  end

  def author
    User.find_by_id(@user)
  end

  def question
    Question.find_by_id(@question)
  end

  def parent_reply
    Reply.find_by_id(@root_reply)
  end

  def child_reply
    QuestionsDatabase.instance.execute(<<-SQL, @reply_id)
    SELECT *
    FROM replies
    WHERE replies.root_reply = ?
    SQL
  end

end

class QuestionFollowers
  attr_reader :user_id, :question_id

  def initialize
    @user_id = options["user_id"]
    @question_id = options["question_id"]
    create
  end

  def create
    QuestionsDatabase.instance.execute(<<-SQL, @user_id, @question_id)
      INSERT INTO question_followers (user_id, question_id)
      VALUES (?, ?)
    SQL
  end

  def self.find_by_user_id(user_id)
    QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT *
    FROM question_followers
    WHERE question_followers.user_id = ?
    SQL
  end

  def self.find_by_question_id(question_id)
    QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT *
    FROM question_followers
    WHERE question_followers.question_id = ?
    SQL
  end

  def self.followers_for_question_id(question_id)
    QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT users.fname, users.lname
    FROM users
    JOIN question_followers ON users.id = question_followers.user_id
    WHERE question_followers.question_id = ?
    SQL
  end

  def self.followed_questions_for_user_id(user_id)
    QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT questions.question_id, questions.title
    FROM questions
    JOIN question_followers ON questions.question_id = question_followers.question_id
    WHERE question_followers.user_id = ?
    SQL
  end

  def self.most_followed_questions(n)
    QuestionsDatabase.instance.execute(<<-SQL, n)
    SELECT question_id, COUNT(question_id)
    FROM question_followers
    GROUP BY question_id
    ORDER BY COUNT(question_id) DESC
    LIMIT ?
    SQL
  end

  def self.most_followed(n)
    Question.most_followed_questions(1)
  end

end

class QuestionLikes
  attr_reader :user_id, :question_id

  def initialize
    @user_id = options["user_id"]
    @question_id = options["question_id"]
    create
  end

  def create
    QuestionsDatabase.instance.execute(<<-SQL, @user_id, @question_id)
      INSERT INTO question_likes (user_id, question_id)
      VALUES (?, ?)
    SQL
  end

  def self.find_by_user_id(user_id)
    QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT *
    FROM question_likes
    WHERE question_likes.user_id = ?
    SQL
  end

  def self.find_by_question_id(question_id)
    QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT *
    FROM question_likes
    WHERE question_likes.question_id = ?
    SQL
  end

  def self.likers_for_question_id(question_id)
    QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT users.fname, users.lname
    FROM users
    JOIN question_likes ON users.id = question_likes.user_id
    WHERE question_likes.question_id = ?
    SQL
  end

  def self.num_likes_for_question(question_id)
    QuestionsDatabase.instance.execute(<<-SQL, question_id)
    SELECT question_id, COUNT(user_id) count
    FROM question_likes
    WHERE question_id = ?
    SQL
  end

  def self.liked_questions_for_user_id(user_id)
    QuestionsDatabase.instance.execute(<<-SQL, user_id)
    SELECT user_id, question_id
    FROM question_likes
    WHERE user_id = ?
    SQL
  end

  def self.most_liked_questions(n)
    QuestionsDatabase.instance.execute(<<-SQL, n)
    SELECT question_id, COUNT(question_id)
    FROM question_likes
    GROUP BY question_id
    ORDER BY COUNT(question_id) DESC
    LIMIT ?
    SQL
  end

end

class Tag
  def self.most_popular
    QuestionsDatabase.instance.execute(<<-SQL, n)
    SELECT *
    FROM
    (SELECT tags.tag_id as tagid, tags.tag_content, questions.title, COUNT(question_tags.question_id) AS count
    FROM tags, question_tags, question_likes, questions
    WHERE tags.tag_id = question_tags.tag_id
    AND question_tags.question_id = question_likes.question_id
    AND question_likes.question_id = questions.question_id
    GROUP BY questions.question_id, tags.tag_id
    )
    GROUP BY tagid
    HAVING MAX(count)
    SQL
  end
end
