CREATE TABLE users (
id INTEGER PRIMARY KEY AUTOINCREMENT,
fname VARCHAR(20) NOT NULL,
lname VARCHAR(20) NOT NULL);

CREATE TABLE questions (
question_id INTEGER PRIMARY KEY AUTOINCREMENT,
title VARCHAR(50) NOT NULL,
body TEXT NOT NULL,
user_id INTEGER NOT NULL,
FOREIGN KEY (user_id) REFERENCES users(id));

CREATE TABLE question_followers (
user_id INTEGER NOT NULL,
question_id INTEGER NOT NULL,
FOREIGN KEY (user_id) REFERENCES users(id),
FOREIGN KEY (question_id) REFERENCES questions(question_id));

CREATE TABLE replies (
reply_id INTEGER PRIMARY KEY AUTOINCREMENT,
user_id INTEGER NOT NULL,
question_id INTEGER NOT NULL,
root_reply INTEGER,
body TEXT NOT NULL,
FOREIGN KEY (user_id) REFERENCES users(id),
FOREIGN KEY (question_id) REFERENCES questions(question_id)
);

CREATE TABLE question_likes(
user_id INTEGER NOT NULL,
question_id INTEGER NOT NULL,
FOREIGN KEY (user_id) REFERENCES users(id),
FOREIGN KEY (question_id) REFERENCES questions(question_id)
);

CREATE TABLE tags(
tag_id INTEGER PRIMARY KEY AUTOINCREMENT,
tag_content TEXT NOT NULL
);

CREATE TABLE question_tags(
tag_id INTEGER NOT NULL,
question_id INTEGER NOT NULL,
FOREIGN KEY (tag_id) REFERENCES tags(tag_id),
FOREIGN KEY (question_id) REFERENCES questions(question_id)
);


INSERT INTO users (fname,lname) VALUES ('Zach', 'Westlake');
INSERT INTO users (fname,lname) VALUES ('Benjamin', 'Lee');
INSERT INTO users (fname,lname) VALUES ('Kush', 'Patel');
INSERT INTO users (fname,lname) VALUES ('Ned', 'Ruggeri');

INSERT INTO questions (title, body, user_id)
VALUES ('How does the program work?', 'Curious how long the
program is', 1);
-- INSERT INTO questions('What will I learn?', 'What subjects will we cover?', 2);
-- INSERT INTO questions('What happens after App Academy?', 'I\'m wondering
-- how long it takes for people to find a job.', 3);
-- INSERT INTO questions('Is twelve weeks enough to prepare a beginner to
-- work as a Rails dev?', '--', 4);

INSERT INTO question_followers VALUES (2, 1);
INSERT INTO question_followers VALUES (3, 1);
INSERT INTO question_followers VALUES (4, 1);

INSERT INTO replies (user_id, question_id, root_reply, body) VALUES (2, 1, NULL, 'You start by learning Ruby.');
INSERT INTO replies (user_id, question_id, root_reply, body) VALUES (3, 1, 1, 'You cover Ruby for two weeks and move onto SQL.');
INSERT INTO replies (user_id, question_id, root_reply, body) VALUES (4, 1, 1, 'You learn SQL, Javascript, and then Rails.');

INSERT INTO question_likes VALUES (2, 1);
INSERT INTO question_likes VALUES (3, 1);
INSERT INTO question_likes VALUES (4, 1);
