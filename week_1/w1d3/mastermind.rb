#!/usr/bin/env ruby

class Mastermind

  def self.play
    computer = Computer.new
    player = Player.new
    num_guesses = 0

    # p "This is the secret code: #{computer.secret_code}"
    puts "Welcome to Mastermind!"

    game_over = false
    until game_over
      self.list_pegs
      guess = gets.chomp.upcase

      until player.valid_guess(guess)
        puts "Please enter a valid combination"
        guess = gets.chomp.upcase
      end

      message = computer.compare_code(player.valid_color)
      p message
      puts

      num_guesses += 1
      game_over = true if (message == %w[right right right right] || num_guesses == 10)
    end

    self.game_end(num_guesses)
  end

  def self.list_pegs
    puts "There are six different peg colors:"
    puts "Red (R), Green (G), Blue (B), Yellow (Y), Orange (O), Purple (P)"
    puts "Please enter a four letter combination of pegs, e.g. 'RGBY' (without quotes)"
  end

  def self.game_end(num_guesses)
    if num_guesses == 10
      puts "Game over: out of turns. Please play again =p!"
    else
      puts "Congratulations!! You guessed the secret code!"
    end
  end

end

class Pegs
  COLOR_OPTIONS = %w[R G B Y O P]

  def self.valid_color?(color)
    COLOR_OPTIONS.include?(color)
  end

  def self.generate_random
    random_pegs = []
    random_pegs << 4.times.map { COLOR_OPTIONS.sample }
    random_pegs.flatten
  end
end

class Computer
  attr_reader :secret_code, :secret_freq

  def initialize
    @secret_code = generate_code
    @secret_freq = generate_freq
  end

  def generate_code
    Pegs.generate_random
  end

  def generate_freq
    color_freq = Hash.new(0)
    secret_code.each { |color| color_freq[color] += 1 }
    color_freq
  end

  def compare_code(player_code)
    player_code_freq = Hash.new(0)
    result_array = exact_match(player_code, player_code_freq)
    another_run_through(player_code, player_code_freq, result_array)
  end

  def exact_match(player_code, player_code_freq)
    result_array = [nil, nil, nil, nil]
    player_code.each_with_index do |color, index|
      if color == secret_code[index]
        result_array[index] = "right"
        player_code_freq[color] += 1
      end
    end

    result_array
  end

  def another_run_through(player_code, player_code_freq, result_array)
    player_code.each_with_index do |color, index|
      if result_array[index].nil?
        if secret_code.include?(color) && player_code_freq[color] < secret_freq[color]
          result_array[index] = "halfway"
          player_code_freq[color] += 1
        else
          result_array[index] = "wrong" if result_array[index].nil?
        end
      end
    end

    result_array
  end
end



class Player
  attr_accessor :valid_color

  def initialize
    @valid_color = nil
  end

  def valid_guess(user_input)
    user_input_copy = user_input.split("")
    user_input_copy.each do |color|
      if !Pegs.valid_color?(color)
        return false
      end
    end

    self.valid_color = user_input_copy
  end
end

Mastermind.play