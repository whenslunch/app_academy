class Hangman

  def play
    player_one = pick_player(1)
    player_two = pick_player(2)
    
    num_wrong_guesses = 0
    word = player_two.make_word
    puts "word: #{word}"
    until num_wrong_guesses == 6
      puts "Player one, please guess a letter."
      guess = player_one.guess_letter
      puts "guess was: #{guess}"
      letters_count_for_guess = player_two.compare(guess)
      player_one.guesses[guess] = letters_count_for_guess
      player_one.feedback = player_two.feedback
      p player_one.feedback
      
      
      num_wrong_guesses += 1 if letters_count_for_guess == 0
      
      break if player_two.opponent_guessed_word?
      # player_two.compare if 0 num_wrong_guesses += 1
      # player_one.add_to_guessed
    end
    
    if num_wrong_guesses == 6
      puts "filtered list: #{player_one.filtered_words.inspect}"
      puts "guessed letters: #{player_one.guesses.keys.inspect}"
      return "You ran out of guesses!"
    else
      puts "filtered list: #{player_one.filtered_words.inspect}"
      puts "guessed letters: #{player_one.guesses.keys.inspect}"
      return "Congratulations, you guessed the word!"
    end
    
  end
  
  def pick_player(player_num)
    puts "Welcome to Hangman!" if player_num == 1
    puts "Will player one (guessing word) be human or computer?" if player_num == 1
    puts "Will player two (picking word) be human or computer?" if player_num == 2
    puts "Enter 'human' or 'computer'"
    user_picked = false
    
    until user_picked
      user_pick = gets.chomp
      puts
      if user_pick == "human"
        return HumanPlayer.new
      elsif user_pick == "computer"
        return ComputerPlayer.new
      else
        puts "Please enter a valid choice."
      end
    end
  end

end

class HumanPlayer
  attr_reader :length_secret_word
  attr_accessor :secret_word, :guesses, :feedback# , :guesses_count
  # wrong_guesses
  # right_guesses hash letter => count
  
  def initialize
    @secret_word = nil
    # @guesses_count = {}
    @guesses = {}
    @feedback = nil
    # @right_guesses = {}
  end  
  
  def make_word
    puts "Please make a secret word"
    chosen_word = gets.chomp
    # self.feedback = "_" * chosen_word.length).split("")
    self.feedback = ["_"] * chosen_word.length
    self.secret_word = chosen_word
  end
  
  def guess_letter
    guess = gets.chomp
    unless ("a".."z").to_a.include?(guess) || guess == "-"
      puts "Please enter a valid letter"
      guess = gets.chomp
    end
    
    guess
  end
  
  def compare(guess)
    puts "SECRET_WORD: #{secret_word}"
    update_feedback(guess)
    
    secret_word.count(guess)
  end
  
  def update_feedback(guess)
    secret_word.split("").each_with_index do |letter, index|
      # puts "guess: #{guess}"
      #       puts "letter: #{letter}"
      if guess == letter
        self.feedback[index] = letter
      end
    end
    
    feedback
  end
  
  def opponent_guessed_word?
    !feedback.include?("_")
  end
  
end

class ComputerPlayer
  attr_accessor :secret_word, :dictionary_hash, :feedback,
    :guesses, :filtered_words
  
  # wrong_guesses
  # right_guesses hash letter => count
  def initialize
    @dictionary_hash = get_dict
    @secret_word = nil
    # @guesses_count = {}
    @feedback = nil
    @guesses = {}
    @filtered_words = []
  end
  
  def make_word
    random_key = dictionary_hash.keys.sample
    random_word = dictionary_hash[random_key].sample
    # self.feedback = "_" * random_word.length).split("")
    self.feedback = ["_"] * random_word.length
    self.secret_word = random_word
  end
  
  def get_dict
    dict_hash = Hash.new { |hash, key| hash[key] = [] }
    words_array = File.readlines("dictionary.txt").map(&:chomp)
    words_array.each do |word|
      dict_hash[word.length] << word
    end
    
    dict_hash
  end
  
  def compare(guess)
    update_feedback(guess)
    secret_word.count(guess)
  end
  
  def update_feedback(guess)
    puts "secret_word: #{secret_word}"
    secret_word.split("").each_with_index do |letter, index|
      # puts "guess: #{guess}"
#       puts "letter: #{letter}"
      if guess == letter
        feedback[index] = letter
      end
    end
    
    feedback
  end
  
  def guess_made(letter, count)
    guesses_count[letter] = count
  end
  
  def guess_letter
    if guesses.empty?
      guess = ("a".."z").to_a.sample
    else
      # WHY IS EDUCATED GUESS NIL
      # WHEN EDUCATED_GUESS IS NOT DEFINED
      guess = educated_guess
      # puts "educated_guess: #{educated_guess.nil?}"
    end
    
    puts "Computer guessed: #{guess}"
    p guesses
    guess
  end
  
  def educated_guess
    self.filtered_words = dictionary_hash[feedback.length] if filtered_words.empty?
    
    guesses.keys.each do |guess|
      filtered_words.select! do |word|
        word.count(guess) == guesses[guess]
      end
    end
    
    
    
    p filtered_words
    random_filtered_word = filtered_words.sample
    pick_unpicked_letter(random_filtered_word)
    
    # puts "educated guess :#{right_length_words}"
    # right_length_words.
  end
  
  def pick_unpicked_letter(word)
    word.split("").shuffle.each do |letter|
      if !guesses.keys.include?(letter)
        return letter
      end
    end
  end
  
  def opponent_guessed_word?
    !feedback.include?("_")
  end
  
    
  
  
  
  # guess random letter
  # guess educated letter based on filtered list with letters, pick most often
  # appearing letter not already picked
  # access the feedback
  # filter words 
  
  
end