class Hangman
  def self.play
    # player_one = Player.new
    # player_two = Player.new

    puts "Welcome to Hangman!"
    puts "What mode would you like to play? 1 or 2."
    mode = gets.chomp.to_i
    if mode == 1
      self.mode1
    elsif mode == 2
      self.mode2
    elsif mode == 3
    else # mode 4
    end
  end

  # human guesser vs cpu
  def self.mode1
    player = Player.new
    puts player.random_word  #debugging
    puts "Guess a letter."
    user_guess = gets.chomp

    feedback = player.update_feedback(user_guess)
    puts "Secret word: #{feedback.inspect}"

    until !feedback.include?("_")
      puts "Guess a letter."
      user_guess = gets.chomp
      feedback = player.update_feedback(user_guess)
      puts "Secret word: #{feedback.inspect}"
    end
  end

  # cpu guesser vs. human
  def self.mode2
    #human player picks word
    #computer player knows length of secret word
    #computer player filters words of only that length
    #computer player random guess

    #loop until win or game over
    #computer player filters again based off of feedback
    #computer player educated guess

    puts "Pick a word for the computer to guess: "
    human = Player.new(gets.chomp)

    cpu = Player.new
    cpu.filter_words_initial(human.human_word.length)
    cpu_guess =  cpu.guess_random_letter
    puts "The computer guessed #{cpu_guess}."

    cpu.wrong_guesses << cpu_guess unless human.human_word.include?(cpu_guess)

    feedback = human.update_feedback(cpu_guess)
    cpu.feedback = feedback
    cpu.filter_words_after_guess
    p feedback

    until !feedback.include?("_")
      cpu_guess = cpu.educated_guess
      #p "guessed letters: #{cpu.guessed_letters.sort}"
      cpu.wrong_guesses << cpu_guess unless human.human_word.include?(cpu_guess)

      puts "The computer guessed the letter: #{cpu_guess}"
      feedback = human.update_feedback(cpu_guess)
      cpu.feedback = feedback
      cpu.filter_words_after_guess
      p feedback
      p "cpu filtered_words count: #{cpu.filtered_words.length}"
    end



  end

  # human vs. human
  def mode3(player)
  end

  # cpu vs cpu
  def mode4
  end
end

class Player
  attr_reader :dictionary
  attr_accessor :feedback, :filtered_words, :human_word, :random_word, :guessed_letters, :wrong_guesses

  def initialize(human_word = nil)
    @dictionary = read_dict
    @random_word = pick_random_word
    @human_word = human_word
    @feedback = create_feedback
    @filtered_words = []
    @guessed_letters = []
    @wrong_guesses = []
  end

  def create_feedback
    if human_word.nil?
      ("_" * random_word.length).split("")
    else
      ("_" * human_word.length).split("")
    end
  end

  def read_dict
    # incorrect functionality
    # dict = Hash.new([])
    
    # correct functionality
    dict = Hash.new { |hash, key| hash[key] = [] } # -__-
    dict_arr = File.readlines("dictionary.txt").map(&:chomp)
    
    dict_arr.each do |word|
      dict[word.length] << word
    end

    dict
  end

  def pick_random_word
    key = dictionary.keys.sample
    word = dictionary[key].sample
  end


  def update_feedback(user_guess)
    self.human_word.split("").each_with_index do |letter, i|
      self.feedback[i] = letter if letter == user_guess
    end
    self.feedback
  end


  #computer methods
  def guess_random_letter
    random_letter = ("a".."z").to_a.sample
    self.guessed_letters << random_letter
    random_letter
  end

  def filter_words_initial(len)
    self.filtered_words = self.dictionary[len]
  end


  def filter_words_after_guess
    # self.filtered_words.select! do |word|
    #   !self.wrong_guesses.sort.join("").include?(word.split(//).sort.join(""))
    # end

    # self.filtered_words.delete_if do |word|
    #   self.guessed_letters.each do |letter|
    #     !self.feedback.include?(letter) && word.include?(letter)
    #   end
    # end
    # self.filtered_words.delete_if do |word|
    #   self.wrong_guesses.each do |letter|
    #     word.include?(letter)
    #   end
    # end
    self.wrong_guesses.each do |letter|
      self.filtered_words.delete_if do |word|
        word.include?(letter)
      end
    end

    self.feedback.each do |letter|
      next if letter == "_"
      self.filtered_words.select! do |word|
        word.count(letter) == self.feedback.count(letter)
      end
    end
    puts "filtered words after guess: #{self.filtered_words}"
  end


  def educated_guess
    possible_word = self.filtered_words.sample
    puts "possible_word: #{possible_word}"
    possible_word.split(//).each do |letter|
      if !guessed_letters.include?(letter)
        guessed_letters << letter
        return letter
      end
    end
  end

end
# hangman = HumanPlayer.new
# guesser = ComputerPlayer.new
# Hangman.new(hangman, guesser)
Hangman.play