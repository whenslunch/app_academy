def most_profitable(stock_prices)
  best_profit = 0
  best_pair = []

  stock_prices.each_with_index do |price, day|
    stock_prices[day..-1].each_with_index do |second_price, second_day|
      if second_price - price > best_profit
        best_profit = second_price - price
        best_pair = [day, second_day]
      end
    end
  end

  best_pair
end

sample_prices = [1, 5, 10, 80, 100, 2, 5]
p most_profitable(sample_prices).inspect