class Fixnum
  def in_words
    number_words = []
    base = 1000
    power = 0
    self_copy = self

    until self_copy == 0
      number_words << parse_3(self_copy % 1000, power)
      self_copy /= 1000
      power += 1
    end

    number_words.reverse.join(" ").rstrip

  end

  def parse_3(number, power)
    num_to_word = {
      0 => "",
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine",
      10 => "ten",
      11 => "eleven",
      12 => "twelve",
      13 => "thirteen",
      14 => "fourteen",
      15 => "fifteen",
      16 => "sixteen",
      17 => "seventeen",
      18 => "eighteen",
      19 => "nineteen",
      20 => "twenty",
      30 => "thirty",
      40 => "fourty",
      50 => "fifty",
      60 => "sixty",
      70 => "seventy",
      80 => "eighty",
      90 => "ninety",
    }

    powers = {
      0 => "",
      1 => "thousand",
      2 => "million",
      3 => "billion",
      4 => "trillion"
    }

    string_array = []

    if number < 1
      return string_array # essentially: ""
    elsif number < 10
      string_array << num_to_word[number]
    elsif number < 100
      if num_to_word.keys.include?(number)
        string_array << num_to_word[number]
      else
        tens = number / 10 * 10
        ones = number % 10
        string_array << num_to_word[tens] << num_to_word[ones]
      end
    else
      hundreds = num_to_word[number / 100].to_s + " hundred"
      tens = num_to_word[number % 100 / 10 * 10]
      ones = num_to_word[number % 10]
      string_array << hundreds << tens << ones
    end

    string_array << powers[power]
    string_array.delete_if {|x| x.empty?}
    string_array.join(" ").rstrip
  end


end

  p 1023.in_words
  p 45.in_words
  p 10_000_000_000.in_words
  p 12_345_678_900.in_words
  p 21.in_words
  p 13.in_words
  p 901.in_words
  p 20.in_words
  p 100_900.in_words