def num_to_s(number, base)
  digit_values = { 0 => "0", 1 => "1", 2 => "2", 3 => "3", 4 => "4",
    5 => "5", 6 => "6", 7 => "7", 8 => "8", 9 => "9", 10 => "A",
    11 => "B", 12 => "C", 13 => "D", 14 => "E", 15 => "F" }
    
    return "0" if number == 0
    
    answer = ""
    power = 0
    while base ** power <= number
      answer += digit_values[(number / (base ** power)) % base]
      power += 1
    end

    answer.reverse
end

p num_to_s(234, 2)

p num_to_s(234, 10)

p num_to_s(0, 2)

