def median(sorted_nums)
  sorted_nums = sorted_nums.sort
  if sorted_nums.length % 2 == 1
    sorted_nums[sorted_nums.length/2]
  else
    (sorted_nums[sorted_nums.length/2 - 1] + 
      sorted_nums[sorted_nums.length/2]) / 2.0
  end
end

p median([1,2,3])

p median([1,2,3,4])

p median([4,1,2,3])