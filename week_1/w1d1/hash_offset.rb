def correct_hash(wrong_hash)
  correct_hash = {}
  wrong_hash.each do |key, value|
    correct_hash[(((key.to_s.ord - 96 + 1) % 26) + 96).chr] = value
  end
  
  correct_hash
end

p correct_hash({ :a => "banana", :b => "cabbage", :c => "dental_floss", :d => "eel_sushi" })
p correct_hash({ :z => "apple" })