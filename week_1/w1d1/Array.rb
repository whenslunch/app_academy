class Array
  def my_each(&prc)
    element = 0
    while element < self.length
      prc.call(self[element])
      element += 1
    end

    self
  end
end

return_value = [1, 2, 3].my_each do |num|
  puts num
end.my_each do |num|
  puts num
end


p return_value