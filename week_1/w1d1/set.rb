def set_add_el(set, element)
  set[element] = true
  set
end

def set_remove_el(set, element)
  set.delete(element)
  set
end

def set_list_els(set)
  set.keys
end

def set_member?(set, element)
  set[element].nil? ? false : true
end

def set_union(set1, set2)
  set1.merge(set2)
end

def set_intersection(set1, set2)
  new_set = {}
  set1.keys.each do |key|
    if set2.has_key?(key)
      new_set[key] = true
    end
  end
  new_set
end

def set_minus(set1, set2)
  new_set = {}
  set1.keys.each do |key|
    if !set2.has_key?(key)
      new_set[key] = true
    end
  end
  new_set
end

set = {:z => true}

set = set_add_el(set, :x) # => make this return {:x => true}
p set.inspect
set = set_add_el(set, :x) # => {:x => true} # This should automatically work if the first method worked
p set.inspect
set = set_remove_el(set, :x) # => {}
p set.inspect
p set_list_els(set) # => [:x, :y]

p set_member?(set, :x) # => false

set = set_union(set, {:y => true}) # => {:x => true, :y => true}
p set.inspect
set = set_intersection(set, {:y => true} ) # I'm not going to tell you how the last two work
p set.inspect
set = set_minus(set, {:y => true} ) # Return all elements of the first array that are not in the second array, not vice versa
p set.inspect