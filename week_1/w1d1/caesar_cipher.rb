def caesar_cipher(phrase, shift)
  ciphered_phrase = ""

  phrase.split.each do |word|
    ciphered_phrase += shift_word(word, shift)
  end

  ciphered_phrase
end

def shift_word(word, shift)
  new_word = ""
  word.split("").each do |letter|
    new_word += (((letter.ord - 96 + shift) % 26) + 96).chr
  end

  new_word
end

# AA solution, clearer
# def shift_word(word, shift)
#   new_word = ""
#   word.each_byte do |ascii|
#     # letter_pos is the position of the letter in the alphabet
#     # "a" is the zeroth letter
#     letter_pos = ascii - "a".ord
#     
#     # use modulo to avoid overshifting; need to wrap!
#     shifted_letter_pos = (letter_pos + shift) % 26
#     
#     # convert back to string format
#     new_word << ("a".ord + shifted_letter_pos).chr
#   end
#   
#   new_word
# end

p caesar_cipher("hello", 3)
p caesar_cipher("zzz", 28)