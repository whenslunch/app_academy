
def hanoi(disc_count)
  stacks = [(1..disc_count).to_a.reverse, [], []]


  until game_complete?(stacks)
    user_input = []

    puts stacks.inspect
    puts "Which numbered stack would you like to move from?"
    user_input << gets.chomp.to_i - 1

    puts "Which numbered stack would you like to move to?"
    user_input << gets.chomp.to_i - 1

    if legal_move?(stacks[user_input[0]],stacks[user_input[1]])
      execute_move(stacks[user_input[0]],stacks[user_input[1]])
    else
      puts "Invalid move. Please try again.\n"
    end

  end
  puts "Congratulations! You've solved the tower of hanoi!"

end


def legal_move?(stack1, stack2)
  if stack1.empty?
    false
  elsif stack2.empty?
    true
  else
    stack1.last < stack2.last
  end

end


def execute_move(stack1, stack2)
  stack2 << stack1.pop
end

def game_complete?(stacks)
  stacks[0].empty? && stacks[1].empty?
end


puts "How many discs would you like to begin with?"
hanoi(gets.chomp.to_i)