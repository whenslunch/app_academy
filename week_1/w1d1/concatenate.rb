def concatenate(str_array)
  str_array.inject("") do |total, letter|
    # don't reassign total since total is a local param to the block
    # don't modify total since the return value is used for next iteration
    total + letter
  end
end

p concatenate(["Yay ", "for ", "strings!"])