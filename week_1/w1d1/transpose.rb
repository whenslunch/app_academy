def transpose(rows)
  columns = []

  rows.length.times do |counter|
    columns[counter] = []

    rows.each do |row|
      columns[counter] << row[counter]
    end
  end

  columns
end

# AA solution

def transpose_aa(rows)
  dimension = rows.first.count
  cols = Array.new(dimension) do
    Array.new(dimension)
  end

  dimension.times do |i|
    dimension.times do |j|
      cols[j][i] = rows[i][j]
    end
  end

  cols
end

rows = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8]
  ]

p transpose(rows).inspect