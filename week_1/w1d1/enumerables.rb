def multiply_by_two(int_array)
  int_array.map { |int| int * 2 }
end

p multiply_by_two([-1,0,1,2])