class Array
	def my_uniq
		uniques = []

		self.each do |num|
			uniques << num unless uniques.include?(num)
		end

		uniques
	end

  def two_sum
    sorted_pairs = []

    first_index = 0
    while first_index < self.length - 1
      second_index = first_index + 1

      while second_index < self.length
        sorted_pairs << [first_index, second_index] 
          if self[first_index] + self[second_index] == 0
        second_index += 1
      end

      first_index += 1
    end

    sorted_pairs
  end
end



p [1, 2, 3, 2, 5].my_uniq == [1, 2, 3, 5]
p [-1, 0, 2, -2, 1].two_sum
