def remix(drinks)
  # alcohol = []
  # mixers = []

  # drinks.each do |drink|
  #   alcohol << drink[0]
  #   mixers << drink[1]
  # end

  alcohols = drinks.map(&:first)
  mixers = drinks.map(&:last)
  
  alcohols.shuffle!
  mixers.shuffle!

  # [
  #   [alcohol[0], mixers[0]],
  #   [alcohol[1], mixers[1]],
  #   [alcohol[2], mixers[2]]
  # ]
  
  [].tap do |new_drinks|
    alcohols.count.times do |i|
      new_drinks << [alcohols[i], mixers[i]]
    end
  end
end

p remix([
  ["rum", "coke"],
  ["gin", "tonic"],
  ["scotch", "soda"]
])