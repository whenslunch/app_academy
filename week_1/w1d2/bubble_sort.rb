def bubble_sort(input_array)
  input_copy = input_array.dup

  counter = input_copy.length - 1
  counter.downto(1) do |count|
    input_copy.each_with_index do |value, index|
      break if index == count
      if input_copy[index] > input_copy[index + 1]
        input_copy[index], input_copy[index + 1] =
        input_copy[index + 1], input_copy[index]
      end
    end
  end

  input_copy
end


p bubble_sort([5,4,3,2,1])
p bubble_sort([2])