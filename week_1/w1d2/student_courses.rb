require 'set'
class Student
  attr_reader :courses
  # same as
  # def courses
  #   @courses
  # end
  
  def initialize(first, last)
    @first, @last = first.capitalize, last.capitalize
    @courses = []
  end

  def name
    @first + " " + @last
  end

  def enroll(course)
    if !@courses.include?(course)
      self.courses.each do |enrolled_course|
        raise "Conflicting course time" 
          if enrolled_course.conflicts_with?(course)
      end
      @courses << course
      course.add_student(self)
    end
  end

  def course_load
    course_credit_hash = {}
    @courses.each do |course|
      course_credit_hash[course.name] = course.credits
    end
    course_credit_hash
  end
end

class Course
  attr_reader :name, :credits, :enrolled_students, :time_block, :days_of_week
  
  def initialize(name, department, credits, days_of_week, time_block)
    @name = name
    @department = department
    @credits = credits
    @enrolled_students = []
    @days_of_week = Set.new[days_of_week]
    @time_block = time_block
  end
  
  def add_student(student)
    if !@enrolled_students.include?(student)
      @enrolled_students << student
      student.enroll(self)
    end
  end
  
  def conflicts_with?(course)
    return false if !self.days_of_week.intersection(course.days_of_week)
    return false if !self.time_block == course.time_block

    true
    
  end
end
