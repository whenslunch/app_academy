def super_print(str, options = {} )
  changed_str = str.dup

  defaults = {
    :times => 1,
    :upcase => false,
    :reverse => false
  }

  options = defaults.merge(options)

  changed_str.upcase! if options[:upcase]
  changed_str.reverse! if options[:reverse]

  options[:times].times do
    puts changed_str
  end
end

super_print("Hello")                                    #=> "Hello"
super_print("Hello", :times => 3)                       #=> "Hello" 3x
super_print("Hello", :upcase => true)                   #=> "HELLO"
super_print("Hello", :upcase => true, :reverse => true) #=> "OLLEH"