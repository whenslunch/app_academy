def guessing_game
  right_guess = false
  cpu_num = rand(1..100)
  guesses = 0

  until right_guess
    puts "Please guess the number between 1 and 100"
    user_guess = gets.chomp.to_i
    guesses += 1
    
    
    # if user_guess > cpu_num
    #   puts "Too high\n"
    # elsif user_guess < cpu_num
    #   puts "Too low\n"
    # else
    #   puts "Your guess is correct!"
    #   puts "You have guessed #{guesses} many times\n"
    #   right_guess = true
    # end
    
    case user_guess <=> cpu_num
    when -1
      puts "Too low\n"
    when 0
      puts "Right guess!"
      puts "Number of guesses: #{guesses}"
    when 1
      puts "Too high\n"
    end
    
    
  end
end

guessing_game