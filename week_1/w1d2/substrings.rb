require 'set'

def substrings(word)
  substrings_array = []

  word.split("").each_with_index do |letter, index|
    substring = ""
    inner_traverser = index
    while inner_traverser < word.length
      substring += word[inner_traverser]
      
      substrings_array << substring unless substrings_array.include?(substring)

      inner_traverser += 1
    end
  end

  substrings_array
end

# AA solution
def substrings(string)
  subs = []
  
  string.length.times do |sub_start|
    ((sub_start + 1 )..string.length).each do |sub_end|
      sub = string[sub_start...sub_end]
      
      subs << sub unless subs.include?(sub)
    end
  end
  
  subs
end

p substrings("cat")

def subwords(word, filename)
  word_set = Set.new
  substrings_array = substrings(word)
  File.foreach(filename) do |line|
    word_set.add(line.chomp)
  end
  
  # delete_if modifies the original array
  substrings_array.delete_if { |substring| !word_set.include?(substring) }
end

p subwords("cat", "./dictionary.txt")