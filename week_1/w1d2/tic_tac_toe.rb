class GameBoard
  def initialize
    @state = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]]
  end

  def state
    @state
  end

  def state=(new_state)
    @state = new_state
  end

  def show_state
    puts "#{state[0][0]}   #{state[0][1]}   #{state[0][2]}"
    puts "#{state[1][0]}   #{state[1][1]}   #{state[1][2]}"
    puts "#{state[2][0]}   #{state[2][1]}   #{state[2][2]}"
  end

  def show_player_indices
    puts '"[0, 0]"   "[0, 1]"   "[0, 2]"'
    puts '"[1, 0]"   "[1, 1]"   "[1, 2]"'
    puts '"[2, 0]"   "[2, 1]"   "[2, 2]"'
  end

  def play
    puts "Welcome to checkers! Please specify the number of human players: 0, 1, 2."
    num_of_humans = gets.chomp.to_i

    if num_of_humans == 2
      puts "Name of Player 1:"
      player_1 = HumanPlayer.new(gets.chomp, "X")
      puts "Name of Player 2:"
      player_2 = HumanPlayer.new(gets.chomp, "O")
    elsif num_of_humans == 1
      puts "Name of Player 1:"
      player_1 = HumanPlayer.new(gets.chomp, "X")
      player_2 = ComputerPlayer.new("Computer", "O")
    else
      player_1 = ComputerPlayer.new("Computer_1", "X")
      player_2 = ComputerPlayer.new("Computer_2", "O")
    end

    game_finished = false
    current_player_turn = player_1
    moves = 0
    until game_finished
      if moves == 9
        puts "Game ended in a draw"
        break
      end

      puts "#{current_player_turn.name}, make your move"
      if current_player_turn.class == HumanPlayer
        show_player_indices
        puts "Pick horizontal row: 0, 1, 2"
        horiz_choice = gets.chomp.to_i
        puts "Pick vertical row: 0, 1, 2"
        vert_choice = gets.chomp.to_i
        next if !current_player_turn.make_move(self, [horiz_choice, vert_choice])
        current_player_turn.make_move(self, [horiz_choice, vert_choice])
      else
        current_player_turn.make_move(self)
      end

      if game_finished?(current_player_turn.mark)
        puts "#{current_player_turn.name} won"
        show_state
        break
      end

      if current_player_turn == player_1
        current_player_turn = player_2
      else
        current_player_turn = player_1
      end

      show_state
      moves += 1
    end
  end

  def game_finished?(player_mark)
    return true if horizontal_win?(player_mark, false)
    return true if vertical_win?(player_mark)
    return true if diagonal_win?(player_mark)
    false
  end

  def horizontal_win?(player_mark, identifier)
    self.state.each_with_index do |horiz_line, horiz_index|
      current_player_pieces = 0
      horiz_line.each_with_index do |vert, vert_index|
        state_of_spot = self.state[horiz_index][vert_index]
        if state_of_spot == player_mark
          current_player_pieces += 1
        end
      end

      return true if current_player_pieces == 3
    end

    if identifier
      self.state = self.state.transpose
    end

    false
  end

  def vertical_win?(player_mark)
    self.state = self.state.transpose
    horizontal_win?(player_mark, true)
  end

  def diagonal_win?(player_mark)
    left_diagonal = [self.state[0][0], self.state[1][1], self.state[2][2]]
    right_diagonal = [self.state[0][2], self.state[1][1], self.state[2][0]]

    return check_diagonal(left_diagonal, player_mark) if check_diagonal(left_diagonal, player_mark)
    return check_diagonal(right_diagonal, player_mark) if check_diagonal(right_diagonal, player_mark)

    false
  end

  def check_diagonal(diagonal, player_mark)
    current_player_pieces = 0

    diagonal.each do |spot|
      if !spot.nil? && spot == player_mark
        current_player_pieces += 1
      end
    end

    if current_player_pieces == 3
      true
    else
      false
    end
  end

end

class HumanPlayer
  def initialize(player_name, player_mark)
    @name = player_name
    @mark = player_mark
  end

  def name
    @name
  end

  def mark
    @mark
  end

  def make_move(board, move)
    if (board.state[move[0]][move[1]]).nil?
      board.state[move[0]][move[1]] = self.mark
    else
      "Invalid move, please try again."
      return false
    end
  end
end





class ComputerPlayer
  def initialize(cpu_name, cpu_mark)
    @name = cpu_name
    @mark = cpu_mark
  end

  def name
    @name
  end

  def mark
    @mark
  end

  def make_move(board)
    move_to_make = winning_move_avail(board)

    move_to_make = random_empty_spot(board) if !move_to_make
    board.state[move_to_make[0]][move_to_make[1]] = self.mark
  end

  def winning_move_avail(board)
    return horizontal_win_avail(board, false) if horizontal_win_avail(board, false)
    return vertical_win_avail(board) if vertical_win_avail(board)
    return diagonal_win_avail(board) if diagonal_win_avail(board)

    false
  end

  def horizontal_win_avail(board, identifier)
    board.state.each_with_index do |horiz_line, horiz_index|
      count_cpu_pieces = 0
      empty_spot = []
      horiz_line.each_with_index do |vert, vert_index|
        state_of_spot = board.state[horiz_index][vert_index]
        if !state_of_spot.nil? && state_of_spot != self.mark
          break
        elsif board.state[horiz_index][vert_index] == self.mark
          count_cpu_pieces += 1
        else
          empty_spot = [horiz_index, vert_index]
        end
      end

      if count_cpu_pieces == 2
        return empty_spot
      end
    end

    if identifier
      board.state = board.state.transpose
    end

    false
  end

  def vertical_win_avail(board)
    board.state = board.state.transpose
    horizontal_win_avail(board, true)
  end

  def diagonal_win_avail(board)
    left_diagonal = { board.state[0][0] => [0, 0], board.state[1][1] => [1, 1], board.state[2][2] => [2, 2] }
    right_diagonal = { board.state[0][2] => [0, 2], board.state[1][1] => [1, 1], board.state[2][0] => [2, 0] }

    return check_diagonal(left_diagonal) if !check_diagonal(left_diagonal)
    return check_diagonal(right_diagonal) if !check_diagonal(right_diagonal)

    false
  end

  def check_diagonal(diagonal)
    count_cpu_pieces = 0
    empty_spot = nil

    diagonal.keys.each do |spot|
      if !spot.nil? && spot != self.mark
        break
      elsif spot == self.mark
        count_cpu_pieces += 1
      else
        empty_spot = diagonal[spot]
      end
    end

    if count_cpu_pieces == 2
      empty_spot
    else
      false
    end
  end

  def random_empty_spot(board)
    empty_spots = []

    board.state.each_with_index do |horiz_line, horiz_index|
      horiz_line.each_with_index do |vert_line, vert_index|
        if board.state[horiz_index][vert_index].nil?
          empty_spots << [horiz_index, vert_index]
        end
      end
    end

    empty_spots.sample
  end
end

game = GameBoard.new
game.play