def file_shuffle
  puts "Please enter the file name."
  file_name = gets.chomp
  original_lines = File.read(file_name).split("\n")
  randomized_lines = original_lines.shuffle.join("\n")

  File.open("#{file_name}-shuffled.txt", "w") do |new_file|
    new_file.puts(randomized_lines)
  end
end

# AA solution
def file_shuffle(filename)
  # creates file if not already present with "w" - write
  File.open("#{filename}-shuffled.txt", "w") do |f|
    # readlines returns an array with each line
    File.readlines(filename).shuffle.each { |line| f.puts line.chomp }
  end
end

file_shuffle("wall_of_text.txt")