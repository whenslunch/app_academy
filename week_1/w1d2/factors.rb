# def factors(num)
#   n = 1
#   factors_array = []
#   while n <= num / 2
#     if num % n == 0
#       factors_array << n
#     end
#     n += 1
#   end
#   factors_array << num
# end

# better solution
def factors(num)
  factors = (1..num/2).select { |i| num % i == 0 }
  factors << num
end

p factors(10)