def rpn_calculator(options={})
  default = {
    :file_name => nil
  }

  options = default.merge(options)
  stack = []

  if options[:file_name]
    stack = File.read(options[:file_name]).split
    final_value = calculate_input_file(stack)
  else
    while true
      puts "Please enter an operand or operator."
      user_input = gets.chomp
      calculate_input(stack, user_input)
    end
  end

end

def calculate_input(stack, input)
  operators = %w[+ - * /]
  if stack.length < 2 && operators.include?(input)
    puts "Illegal entry, not enough operands"
    return
  elsif input == "+"
    stack << stack.pop + stack.pop
  elsif input == "-"
    second_number = stack.pop
    stack << stack.pop - second_number
  elsif input == "*"
    stack << stack.pop * stack.pop
  elsif input == "/"
    second_number = stack.pop
    stack << stack.pop / second_number
  else
    stack << input.to_i
    return
  end

  puts "Your operation returned #{stack.last}"
end

def calculate_input_file(stack)
  final_stack = []
  stack.reverse!
  until stack.empty?
    calculate_input(final_stack, stack.pop)
  end
  final_stack.first
end

if __FILE__ == $PROGRAM_NAME
  rpn_calculator
end