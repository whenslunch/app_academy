def rps(user_choice)
  choices = ["Rock", "Paper", "Scissors"]
  computer_choice = choices.sample

  if user_choice == computer_choice
    return "#{user_choice} == #{computer_choice} Draw"
  end
  
  # DRY
  # elsif user_choice == "Rock"
  #   if computer_choice == "Paper"
  #     "#{computer_choice}, Loss"
  #   else
  #     "#{computer_choice}, Win"
  #   end
  # elsif user_choice == "Paper"
  #   if computer_choice == "Rock"
  #     "#{computer_choice}, Win"
  #   else
  #     "#{computer_choice}, Loss"
  #   end
  # else
  #   if computer_choice == "Rock"
  #     "#{computer_choice}, Loss"
  #   else
  #     "#{computer_choice}, Win"
  #   end
  # end
  
  wins = [["Rock", "Scissors"], ["Paper", "Rock"], ["Scissors", "Paper"]]
  if wins.include?([user_choice, computer_choice])
    "#{user_choice} > #{computer_choice}, Win"
  else
    "#{user_choice} > #{computer_choice}, Lose"
  end
end


p rps("Paper")
p rps("Rock")
p rps("Scissors")