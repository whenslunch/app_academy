def select_nums
  number_not_found = true
  n = 0
  while number_not_found
    if n > 250 and n % 7 == 0
      puts n
      break
    end
    n += 1
  end
end

# AA solution
def loop_through_nums
  i = 0
  
  until (i > 250) && (i % 7 == 0)
    i += 1
  end
  
  i
end

select_nums