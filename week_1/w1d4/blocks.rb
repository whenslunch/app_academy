class Array
  def my_each(&prc)
    self.count.times do |item|
      prc.call(self[item])
    end
    self
  end

  def my_map(&prc)
    new_arr = []
    self.my_each do |item|
      new_arr << prc.call(item)
    end
    new_arr
  end

  def my_select(&prc)
    new_array = []
    self.my_each do |item|
      new_array << item if prc.call(item)
    end

    new_array
  end

  def my_inject(&prc)
    value = self[0]
    self.each_with_index do |item, index|
      next if index == 0
      value = prc.call(value, item)
    end
    value
  end


  def my_sort!(&prc)
    self.length.times do |counter|
      self.each_with_index do |num, index|
        if prc.call(num, self[index + 1]) == 1
          self[index], self[index + 1] = self[index + 1], self[index]
        end
      end
    end
    self
  end

  def pass_many_args(*args, &prc)
    raise "NO BLOCK GIVEN" if prc.nil?
    prc.call(*args)
  end

end
#  array = [1,2,3,4,5]
#  pass_many_args(*array) { |x| puts x + 2 }

array = [1,2,3,4].my_sort! { |x,y| x<=>y }
p array

