def range(start_val, end_val)
  if start_val == end_val#base case
    [end_val]
  else
    [start_val] + range(start_val + 1, end_val)
  end
end

def recursive_sum(array)
  if array.length == 1# base
    array.first
  else
   array[-1] + recursive_sum(array[0...-1])
  end
end

def iterative_sum(array)
  sum = 0
  array.each do |num|
    sum += num
  end
  sum
end

def exp_recurse_1(number, exponent)
  if exponent == 0#base
    1
  else
    number * exp_recurse_1(number, exponent - 1)
  end
end

def exp_recurse_2(number, exponent)
  if exponent == 0
    1
  elsif exponent % 2 == 0
    exp_recurse_2(number, exponent / 2) ** 2
  else
    number * (exp_recurse_2(number, (exponent - 1)) ** 2)
  end
end

# doesn't work : ([1, 2, [3,4], 5])
def deep_dup(array)
  return array if array.is_a?(Fixnum)
  if !array.any? { |element| element.is_a? Array }
    array.dup
  else
    deep_dup(array[0...-1]) + [deep_dup(array.last)]
  end
end

def rec_fib(num)#fib(3)
  if num == 0
    []
  elsif num == 1
    [0]
  elsif num == 2
    [0, 1]
  else
    [rec_fib(num-1), rec_fib(num-1)[-1] + rec_fib(num-1)[-2]].flatten
  end
end

def it_fib(num)
  fib_array = []
  return fib_array if num == 0
  1.upto(num) do |i|
    if i == 1
      fib_array << 0
    elsif i == 2
      fib_array << 1
    else
      fib_array << fib_array[-1] + fib_array[-2]
    end
  end

  fib_array
end


def bsearch(array, target)
  mid_index = array.length/2

  if array.length == 1 && target != array[mid_index]
    nil
  elsif target == array[mid_index]
    mid_index
  elsif target < array[mid_index]
    bsearch(array[0..mid_index], target)
  elsif target >= array[mid_index]
    index = bsearch(array[mid_index..array.length], target)
    index.nil? ? nil : (index + mid_index)
  end
end

# amount 16
def make_change(amount, coins = [10, 7, 1])
  # if amount % coins[0] == 0
  if amount == coins[0]
    [coins[0]]
  elsif amount > coins[0]
    [coins[0]] + make_change(amount - coins[0], coins)

    diff_coins = make_change(amount - coins[0], coins)
    same_coins = [coins[0]] + diff_coins
#    diff_coins = [coins[0]] + make_change(amount - coins[0], coins[1..-1])

    same_coins.length < diff_coins.length ? same_coins : diff_coins
  else
    make_change(amount, coins[1..-1])
  end
end

def merge_sort(array)
  list1=
  list2=

  [] <<

  # if array.length == 1
  #   array.first
  # elsif array.length == 2
  #   if array.last < array.first
  #     array.first, array.last = array.last, array.first
  #   end
  # else
    # merge_sort(merge_sort(array[0...array.length/2]) + merge_sort(array[array.length/2...-1]))
  # end
  first_half = merge_sort(array[0...array.length/2])
  second_half = merge_sort(array[array.length/2..-1])

  array.each_with_index do |item, index|
    item[]
  end
end

def subsets

end