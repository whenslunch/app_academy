# require "./tree.rb"
class TreeNode
  attr_reader :left, :right
  attr_accessor :parent, :value, :visited

  def initialize(parent, value, left=nil, right=nil)
    @parent = parent
    @left = left
    @right = right
    @value = value
    @visited = false
  end

  def left=(value)
    self.remove_left if self.left
    @left = TreeNode.new(self, value)
  end

  def right=(value)
    self.remove_right if self.right
    @right = TreeNode.new(self, value)
  end


  def remove_left
    self.left.parent = nil
    self.left = nil
  end

  def remove_right
    self.right.parent = nil
    self.right = nil
  end

  def self.dfs(node, find_value)
    puts "On node with value #{node.value}"
    return node if node.value == find_value
    left_tree_result = self.dfs(node.left, find_value) if node.left
    return left_tree_result if !left_tree_result.nil?
    right_tree_result = self.dfs(node.right, find_value) if node.right
    return right_tree_result if !right_tree_result.nil?
  end

  def self.bfs(node, find_value)
    array = [node]
    while !array.empty?
      first_node = array.shift
      puts "On node with value #{first_node.value}"
      return first_node if first_node.value == find_value
      array << first_node.left if first_node.left
      array << first_node.right if first_node.right
    end
    nil
  end


end



root = TreeNode.new(nil, 100)
root.left = 50
node50 = root.left
root.right = 110
node110 = root.right
node50.left = 20
node20 = node50.left
node50.right = 70
node70 = node50.right
node110.left = 105
node105 = node110.left

p return_node = TreeNode.dfs(root, 105)
p return_node = TreeNode.bfs(root, 105)



