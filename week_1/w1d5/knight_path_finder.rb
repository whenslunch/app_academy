require './tree_node.rb'

class KnightPathFinder
  attr_accessor :move_tree, :pos

  def initialize(pos = [0,0])
    @pos = pos
    @move_tree = build_move_tree
  end

  def build_move_tree
    root = TreeNode.new(nil, pos)
    array = [root]
    visited_array = [pos]
    until array.empty?
      first_node = array.shift
      generated_pos = KnightPathFinder.new_move_positions(first_node.value)
      generated_pos.delete_if { |pos| visited_array.include?(pos) }
      generated_pos.each do |pos|
        first_node.children = pos
        added_child = first_node.children.last
        array << added_child
      end
      visited_array += generated_pos
    end
    root
  end

  def find_path(target_pos)
     move_tree.path(TreeNode.bfs(move_tree, target_pos))
  end

  def self.new_move_positions(pos)
    potential_moves = []
    x = pos[0]
    y = pos[1]
    potential_moves << [x + 1, y - 2]
    potential_moves << [x + 2, y - 1]
    potential_moves << [x + 1, y + 2]
    potential_moves << [x + 2, y + 1]
    potential_moves << [x - 1, y - 2]
    potential_moves << [x - 2, y - 1]
    potential_moves << [x - 2, y + 1]
    potential_moves << [x - 1, y + 2]

    potential_moves.select! { |array| array[0] >= 0 && array[0] <= 7 &&
                                      array[1] >= 0 && array[1] <= 7 }
    potential_moves
  end

end

kpf = KnightPathFinder.new([0, 0])

node_path = kpf.find_path([7, 7]) # => [[0, 0], [2, 1]]
# p found_node.value
p node_path
# kpf.find_path([3, 3]) # => [[0, 0], [2, 1], [3, 3]]

# p KnightPathFinder.new_move_positions([4, 4])