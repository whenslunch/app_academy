# require "./tree.rb"
class TreeNode
  attr_reader :children
  attr_accessor :parent, :value, :visited, :count

  def initialize(parent, value, children = [])
    @parent = parent
    @children = children
    @value = value
    @visited = false
    @count = 1
  end

  def children=(value)
    child = TreeNode.new(self, value)
    @children << child
    @count += 1
  end



  def remove_child(value)
    self.children.each_with_index do |child, index|
      if child.value == value
        self.children[index].parent = nil
        self.children.delete_at(index)
        break
      end
    end
  end


  def self.dfs(node, &block)
    puts "On node with value #{node.value}"
    return node if block.call(node.value)
    node.children.each do |child|
      child_result = self.dfs(child, &block)
      return child_result unless child_result.nil?
    end
    nil
  end


  def self.bfs(node, &block)
    array = [node]
    until array.empty?
      first_node = array.shift
      # puts "On node with value #{first_node.value}"
      return first_node if block.call(first_node.value)
      if first_node.children
        first_node.children.each do |child|
          array << child
        end
      end
    end
    nil
  end


end

# root = TreeNode.new(nil, 100)
# root.children = 50
# node50 = root.children.last
# root.children = 110
# node110 = root.children.last
# root.children = 70
# node70 = root.children.last
# root.children = 30
# node30 = root.children.last
# node50.children = 5
# node5 = node50.children.last
# node50.children = 10
# node10 = node50.children.last
# node110.children = 8
# node8 = node110.children.last
# node70.children = 40
# node40 = node70.children.last
#
#
# child_result = TreeNode.bfs(root) { |x| x == 40 }
# p child_result.value
#


